<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('casts.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function index(){
        $casts = DB::table('casts')->get();
        return view('casts.index', compact('casts'));
    }

    public function show($cast_id){
        $cast = DB::table('casts')->where('id', $cast_id)->first();
        return view('casts.show', compact('cast'));
    }

    public function edit($cast_id){
        $cast = DB::table('casts')->where('id', $cast_id)->first();
        return view('casts.edit', compact('cast'));
    }

    public function update($cast_id, Request $request){
        $query = DB::table('casts')->where('id', $cast_id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast')->with('success', 'Berhasil Update Cast!');
    }

    public function destroy($cast_id){
        $query = DB::table('casts')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('success', 'Cast Berhasil Dihapus');
    }
}