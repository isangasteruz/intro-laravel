@extends ('adminlte.master')

@section ('content')
<div class="ml-3">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Casts Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <a class="btn btn-primary mb-3" href="/cast/create">Add Cast</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($casts as $key => $cast)
                      <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $cast->nama }}</td>
                          <td>{{ $cast->umur }}</td>
                          <td>{{ $cast->bio }}</td>
                          <td style="display: flex;">
                              <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Show</a>
                              <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                              <form action="/cast/{{$cast->id}}" method="post">
                              @csrf
                              @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                              </form>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>

</div>

@endsection