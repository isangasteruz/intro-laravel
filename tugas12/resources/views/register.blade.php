<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>

	<h2>Sign Up Form</h2>

	<form action="/welcome" method="POST">
	@csrf
		<label>First Name :</label>					<br><br>
		<input type="text" name="nama1"> 			<br><br>

		<label>Last Name :</label>					<br><br>
		<input type="text" name="nama2"> 			<br><br>

		<label>Gender :</label> 					<br><br>
		<input type="radio" name="gender">Male 		<br>
		<input type="radio" name="gender">Female 	<br>
		<input type="radio" name="gender">Other 	<br><br>

		<label>Nationality :</label> 				<br><br>
		<select name="negara">
			<option value="1">Indonesian</option>
			<option value="2">Singaporean</option>
			<option value="3">Malaysian</option>
			<option value="4">Australian</option>
		</select>									<br><br>


		<label>Language Spoken :</label>			<br><br>
		<input type="checkbox">Bahasa Indonesia 	<br>
		<input type="checkbox">English				<br>
		<input type="checkbox">Other				<br><br>

		<label>Bio :</label>						<br><br>
		<textarea name="bio" cols="40" rows="10">
		</textarea>									<br><br>

		<input type="submit">
	</form>

</body>
</html>